import mysql.connector
from mysql.connector import errorcode

config = {
	'user': 'bookengineuser',
	'password': 'QwErTy1@3$',
	'host': '127.0.0.1',
	'database': 'bookengine',
	'raise_on_warnings': True
}

def Access(callback, errcallback):
	try:
		cnx = mysql.connector.connect(**config)
		result = callback(cnx)
	except mysql.connector.Error as err:
		return errcallback(err)
	else:
		cnx.close()
		return result
