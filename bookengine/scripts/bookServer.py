from http.server import BaseHTTPRequestHandler
from scripts.mySqlHandler import Access
from urllib.parse import urlparse
from urllib.parse import unquote
from scripts.editorHandlers import Init as editorInit
import json

handlersLib = dict([])
editorInit(handlersLib)

class BookServer(BaseHTTPRequestHandler):
	def _set_headers(self):
		self.send_response(200)
		self.send_header("Content-type", "text/json; charset=utf-8")
		self.end_headers()

	def _json(self, data):        
		content = json.dumps(data, sort_keys=True, indent=4)
		return content.encode("utf8")

	def do_Action(self, params):
		if "action" not in params:
			return {"result": "error", "message":"No action defined in query"}
		if params["action"] not in handlersLib:
			return {"result": "error", "message": "No handler defined for action "+params["action"]}
		result = Access(lambda connection: handlersLib[params["action"]](connection, params), lambda err: {"result": "error", "message": "Something went wrong during database interaction", "err": err})
		if "err" in result:
			print ("Error: ", result["err"])
			result["err"] = "("+result["err"].errno+")"+result["err"].msg
		return result

	def do_GET(self):
		print("Doing GET")		
		self._set_headers()
		query = urlparse(self.path).query
		params = dict(qc.split("=") for qc in query.split("&"))
		self.wfile.write(self._json(self.do_Action(params)))

	def do_POST(self):
		#print("Doing POST")
		self._set_headers()
		length = int(self.headers['Content-Length'])
		post_data = self.rfile.read(length).decode('utf-8') # <--- Gets the data itself
		post_data = post_data
		params = dict(unquote(qc.replace('+',' ')).split("=") for qc in post_data.split("&"))
		#print("Post parameters arrived:", post_data, params);
		self.wfile.write(self._json(self.do_Action(params)))

	def do_HEAD(self):
		self._set_headers()
		#print("do_Head")


