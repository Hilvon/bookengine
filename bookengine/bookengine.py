from http.server import HTTPServer

from scripts.bookServer import BookServer


def run(server_class=HTTPServer, handler_class=BookServer, addr="localhost", port=5000):
	server_address = (addr, port)
	httpd = server_class(server_address, handler_class)

	print(f"Starting httpd server on {addr}:{port}")
	httpd.serve_forever()

run()