<input id="authkey" name="authkey" type="password" placeholder="ключ редактирования сюда" />
<div class="book-selection-section">
	<div class="book-select-filter"/>
		<input class="book-select-filter-input" id="book-search-title-input" type="text" />
		<button class="w3-button search-button" onclick="bookEditorContext.fetchBookList($('#book-search-title-input').val());">Найти</button>
		<button class="w3-button add-button" onclick="bookEditorContext.CreateBook($('#book-search-title-input').val());"/>Создать</button>
	</div>	
	<div class="book-selection-list-container"></div>
</div>

<div class="book-selected w3-hide">
	<div class="bookEditing">
		<div class="book-edit-title-container book-edit-container">
			Название:
			<input type="text" id="book-edit-title" />
			<button class="w3-button change-title-button" onclick="bookEditorContext.ChangeBookTitle($('#book-edit-title').val())">Изменить</button>
		</div>
		<div class="book-edit-cover-container book-edit-container">
			Обложка:
			<input type="text" id="book-edit-cover" />
			<span class="thumbnail-box book-cover-thumbnail" />
			<button class="w3-button change-title-button" onclick="bookEditorContext.ChangeBookCover($('#book-edit-title').val())">Изменить</button>
		</div>
		<div class="book-edit-description-container book-edit-container">
			Описание:
			<textarea id="book-edit-description"></textarea>
			<button class="w3-button change-title-button" onclick="bookEditorContext.ChangeBookDescription($('#book-edit-description').val())">Изменить</button>
		</div>
	</div>
	<div class="pageEditing">
		<div class="page-list-section">
			Страницы:
			<input class="page-select-filter-input" id="page-search-input" type="text" />
			<button class="w3-button search-button" onclick="bookEditorContext.ListBookPages($('#page-search-input').val());">Найти</button>
			<button class="w3-button search-button" onclick="bookEditorContext.StartNewPage($('#page-search-input').val());">Добавить</button>
			<div class="page-list-container"></div>
		</div>
		<div class="page-details-section">
			Выбранная страница:
			<button class="w3-button set-firstpage-button" onclick="bookEditorContext.SetStartingPage()">Назначить стартовой</button>
			<div class="page-edit-title-container page-edit-container">
				Название:
				<input class="page-hint-input" id="page-hint-input" type="text" />
				<button class="w3-button change-title-button" onclick="bookEditorContext.SetPageName($('#page-hint-input').val())">Изменить</button>
			</div>
			<div class="page-edit-text-container page-edit-container">
				Текст на странице:
				<textarea id="page-edit-description"></textarea>
				<button class="w3-button change-title-button" onclick="bookEditorContext.SetPageText($('#page-edit-description').val())">Изменить</button>
			</div>

			<div class="exitsEditing w3-hide">
				EXITS EDITING HERE
			</div>
		</div>	
	</div>
</div>