function TryLogin() {
    coreModule.Get({
        action: 'verifylogin',
        login: $('#input-login').val()
    });    
}

function LoginMissing(response) {
    $('.register-container').show();
}
coreModule.responseActionHandlers['loginmissing'] = LoginMissing;

function Register() {
    let pwd1 = $('#input-register-password').val();
    if (pwd1 == $('#input-register-password-repeat').val()) {
        coreModule.Post({
            action: 'register',
            login: $('#input-login').val(),
            password: pwd1
        });
    }
}

function LoginAccepted(response) {
    $('.password-container').show();
}
coreModule.responseActionHandlers['loginexists'] = LoginAccepted;


function Authenticated(response) {
    window.location.href = '/?key='+response.userkey;
}
coreModule.responseActionHandlers['authenticated'] = Authenticated;

function TryAuthenticane() {
    coreModule.Post({
        action: 'authenticate',
        login: $('#input-login').val(),
        password: $('#input-password').val()
    });
}

function TryGetCurrentPage() {
    coreModule.Get({
        action: 'getpage',
        userkey: $('#userKey').text()
    });
}

function TryUseExit(exitId) {
    coreModule.Post({
        action: 'useExit',
        userkey: $('#userKey').text(),
        exitid: exitId
    });
}

function TrySelectBook(bookId) {
    coreModule.Post({
        action: 'selectBook',
        userkey: $('#userKey').text(),
        bookId: bookId
    });
}


function OnPageRecieved(response) {

    $('.pageText').html(decodeURI(response.pagetext));
    let exitContainer = $('.pageExitsContainer');
    exitContainer.html('');
    for (let i in response.exits) {
        let exit = response.exits[i];
        let exitInstance = $('<div></div>').addClass('exitOption').html(exit.text);
        exitInstance.on('click', () => { TryUseExit(exit.exitid); });
        exitContainer.appendTo(exitInstance);
    }
}
coreModule.responseActionHandlers['showpage'] = OnPageRecieved;

function OnSelectBook(response) {
    // ��� ����� ������� ������ ����. ����������� �� � ��������� ���� � �������� 
}



$(document).ready(() => {
    console.log('Hello World');
    if ($('#userkey').text()) {
        TryGetCurrentPage();
    } else {
        $('.auth-section').removeClass('w3-hide');
    }
});