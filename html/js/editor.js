let bookEditorContext = {}


function EditorJsInit(coreModule) {

    bookEditorContext.fetchBookList = (filter) => {
        $('.book-selected').fadeOut();
        coreModule.Get({
            action: 'listbooks',
            filter: filter
        });
    }

    bookEditorContext.CreateBook = (bookTitle) => {
        if (bookTitle) {
            coreModule.Post({
                action: 'makebook',
                title: bookTitle,
                authkey: $('#authkey').val()
            });
        }
    }

    let activeBookId = -1;
    let activePageId = -1;

    bookEditorContext.ChangeBookTitle = (newTitle)=>{
        if (activeBookId > 0) {
            coreModule.Post({
                action: 'updatebook',
                bookId: activeBookId,
                title: newTitle,
                authkey: $('#authkey').val()
            });
        }
    }

    bookEditorContext.ChangeBookCover = (newCover) => {
        if (activeBookId > 0) {
            coreModule.Post({
                action: 'updatebook',
                bookId: activeBookId,
                cover: newCover,
                authkey: $('#authkey').val()
            });
        }
    }

    bookEditorContext.ChangeBookDescription = (newDescription) => {
        if (activeBookId > 0) {
            coreModule.Post({
                action: 'updatebook',
                bookId: activeBookId,
                description: newDescription,
                authkey: $('#authkey').val()
            });
        }
    }
    bookEditorContext.ChangeBookStartPage = () => {
        if (activeBookId > 0 && activePageId > 0) {
            coreModule.Post({
                action: 'updatebook',
                bookId: activeBookId,
                startPageId: activePageId,
                authkey: $('#authkey').val()
            });
        }
    }

    bookEditorContext.ListBookPages = (filter) => {
        if (activeBookId > 0) {
            coreModule.Get({
                action: 'getbookpages',
                bookId: activeBookId,
                filter: filter
            });
        }
    }

    bookEditorContext.StartNewPage = (pageName) => {
        if (activeBookId > 0) {
            coreModule.Post({
                action: 'addbookpage',
                bookId: activeBookId,
                pageName: pageName,
                authkey: $('#authkey').val()
            });
        }
    }

    bookEditorContext.SetPageName = () => {
        if (activeBookId > 0) {
            coreModule.Post({
                action: 'setpagedata',
                bookId: activeBookId,
                pageId: activePageId,
                pageName: $("#page-hint-input").val(),
                authkey: $('#authkey').val()
            });
        }
    }

    bookEditorContext.SetPageText = () => {
        if (activeBookId > 0) {
            coreModule.Post({
                action: 'setpagedata',
                bookId: activeBookId,
                pageId: activePageId,
                pageText: $("#page-edit-description").val(),
                authkey: $('#authkey').val()
            });
        }
    }

    let LoadedBooksList = [];
    let ExistingBookWidgets = []

    function SetupThumbnail(thumbnail, bookdata) {
        if (bookdata.cover) {
            thumbnail.css({ "background-image": "url('" + bookdata.cover+"')"})
        }
        return thumbnail;
    }

    function RefreshSelectableBookWidgets() {
        let ConfirmedBookWidgets = [];
        let listContainer = $('.book-selection-list-container');
        for (let i in LoadedBooksList) {
            let bookData = LoadedBooksList[i];
            bookWidget = null;
            if (ExistingBookWidgets[bookData.bookId]) {
                bookWidget = ExistingBookWidgets[bookData.bookId];
                ExistingBookWidgets[bookData.bookId] = null;
            }
            if (!bookWidget) {
                bookWidget = $("<div></div>").addClass("book-select-details-widget").appendTo(listContainer);
                $("<span></span>").addClass('book-details-title').text(bookData.bookTitle).appendTo(bookWidget);
                SetupThumbnail($("<span></span>").addClass('book-details-thumbnail'), bookData).appendTo(bookWidget);
                $("<span></span>").addClass('book-details-description').html(bookData.description).appendTo(bookWidget);

                bookWidget.on("click", () => selectActiveBook(bookData.bookId));
            }
            ConfirmedBookWidgets[bookData.bookId] = bookWidget;
        }
        for (let i in ExistingBookWidgets) {
            killWidget = ExistingBookWidgets[i];
            if (killWidget) {
                killWidget.remove();
            }
        }
        ExistingBookWidgets = ConfirmedBookWidgets;
    }

    function CreatePageListWidget() {
        let $pageWidget = $("<div></div>").addClass("page-select-details-widget").appendTo($('.page-list-container'));
        $pageWidget.$title = $("<span></span>").addClass('page-details-title').appendTo($pageWidget);
        $pageWidget.on("click", () => SetActivePage($pageWidget.id))
        return $pageWidget;
    }

    function SetPageListWidgetData(widget, data) {
        widget.$title.text(data.pageName);
        widget.id = data.pageId;
    }

    let SpawnedPageListWidgets = [];
    let LoadedPageDatas = {};

    function UpdatePageWidgets() {
        let i = 0;
        for (let di in LoadedPageDatas) {
            let data = LoadedPageDatas[di];
            if (SpawnedPageListWidgets.length > i) {
                SetPageListWidgetData(SpawnedPageListWidgets[i].show(), data);
            } else {
                let newWidget = CreatePageListWidget();
                SetPageListWidgetData(newWidget, data);
                SpawnedPageListWidgets.push(newWidget);
            }
            i++;
        }
        
        for (; i < SpawnedPageListWidgets.length; i++) {
            SpawnedPageListWidgets[i].hide();
        }
    }

    function OnBookListReceived(response) {
        LoadedBooksList = [];
        for (let i in response.books) {
            let bookdata = response.books[i];
            LoadedBooksList[bookdata.bookId] = bookdata;
        }
        RefreshSelectableBookWidgets();
    //Parse list of books and present them as a list of selectable books. Or display "None found" if no books returned.
    }

    coreModule.responseActionHandlers['bookslisted'] = OnBookListReceived;

    function selectActiveBook(bookId) {
        if (LoadedBooksList[bookId]) {
            if (bookId != activeBookId) {
                activeBookId = bookId;
                refreshActiveBookDetails();
                $('.book-selected').fadeIn().removeClass('w3-hide');
                bookEditorContext.ListBookPages(activeBookId, '')
            }            
        }
        // Check if this ID is mentioned in loaded book list, if yes - set book Id as active book id and open book editing UI.
    }

    function refreshActiveBookDetails() {
        if (LoadedBooksList[activeBookId]) {
            let bookData = LoadedBooksList[activeBookId];
            $('#book-edit-title').val(bookData.bookTitle);
            $('#book-edit-cover').val(bookData.cover);
            SetupThumbnail($('book-cover-thumbnail'), bookData);
            $('#book-edit-description').val(bookData.description);
        }
    }

    function SetPageData(pageData) {
        LoadedPageDatas[pageData.pageId] = pageData;
    }

    function SetPageDataCollection(pageDatas) {
        LoadedPageDatas = {};
        for (let i in pageDatas) {
            SetPageData(pageDatas[i]);
        }
    }

    function SetActivePage(pageId) {
        let pageData = LoadedPageDatas[pageId];
        activePageId = pageId;
        SetActivePageTexts(pageData);
        //ALSO REQUEST POSSIBLE EXITS
    }

    function SetActivePageTexts(pageData) {
        $("#page-hint-input").val(pageData.pageName);
        $("#page-edit-description").val(pageData.text);
    }

    function OnBookCreated(response) {
        //Add book details to the loaded books list. Refresh book selection UI.
        let bookdata = response.book;
        LoadedBooksList[bookdata.bookId] = bookdata;
        RefreshSelectableBookWidgets();
    }
    coreModule.responseActionHandlers['bookcreateok'] = OnBookCreated;

    function OnBookUpdated(response) {
        //Check if book ID exists in list of remembered books. If it is, update data and redraw UIs
        OnBookCreated(response);
        refreshActiveBookDetails();
    }
    coreModule.responseActionHandlers['bookupdateok'] = OnBookUpdated;

    function BookPagesListed(response) {
        SetPageDataCollection(response.pages);
        UpdatePageWidgets();
    }

    coreModule.responseActionHandlers['bookpageslisted'] = BookPagesListed;

    function DisplayBookDetais(response) {
        SetPageData(response.pageData);
        UpdatePageWidgets();
        if (activePageId == response.pageData.pageId) {
            SetActivePageTexts(response.pageData);
        }
    }
    coreModule.responseActionHandlers['bookpagedetails'] = DisplayBookDetais;
}
EditorJsInit(coreModule);

$(document).ready(function () {

});