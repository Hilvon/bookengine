let coreModule = {
    responseActionHandlers: []
};

coreModule.init = ($)=>{
    
    function HandleServerResponse (response) {
        if (response && response.result && coreModule.responseActionHandlers[response.result])
            coreModule.responseActionHandlers[response.result](response);
        else
            BeConfused(response);
    }

    function ShoutError(response) {
        if (response && response.message) {
            console.log(response.message);
        }
        else {
            console.log('ERROR (No message):');
        }
        console.log(response);
    }

    function BeConfused(response) {
        console.log("Unable to figure out how to handle response:");
        console.log(response);
    }

    function BeSilent(esponse) {
        //DO NOTHING.
    }

    function HandleResponse(callback) {
        return function (response, status) {
            HandleServerResponse(response);
            if (callback)
                callback(response);
        }
    }

    function HandleError(xhttp, status, exception) {
        ShoutError(exception);
    }

    coreModule.Get = function (data, callback) {
        $.ajax({
            url: 'engine/',
            method: 'GET',
            data: data,
            success: HandleResponse(callback),
            error: HandleError
        })
    };

    coreModule.Post = function (data, callback) {
        console.log("We are supposed to send POST request now.");
        $.post({
            url: 'engine/',
            method: 'POST',
            data: data,
            success: HandleResponse(callback),
            error: HandleError
        })
    };

    coreModule.responseActionHandlers['error'] = ShoutError;
    coreModule.responseActionHandlers['silent'] = BeSilent;

    coreModule.init = ($) => {
        console.log("Already initialized...");
    }
};

coreModule.init($);